#!/bin/bash
docker-compose down

docker-compose up -d mysql

docker-compose build
docker-compose up -d api

docker-compose up -d locust_worker
docker-compose up locust_master
