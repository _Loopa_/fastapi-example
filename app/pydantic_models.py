from pydantic import BaseModel
from datetime import date


class UserInfo(BaseModel):
    name: str
    birth_date: date | None = None


class UserWithPassword(UserInfo):
    password: str


class UserModel(UserInfo):
    username: str
    disabled: bool | None = None


class UserInDB(UserModel):
    hashed_password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None
