import uvicorn
from fastapi import FastAPI, Request, Depends
from fastapi.responses import HTMLResponse
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.exceptions import RequestValidationError
from fastapi.templating import Jinja2Templates
from fastapi.exception_handlers import (
    http_exception_handler,
    request_validation_exception_handler,
)
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import json
import logging
from database.db_config import HOST
from database.db_actions import search_by_username, get_table, add_user_in_db, edit_user_in_db
from database.db_template import DatabaseUser
from app.pydantic_models import UserInfo, Token, UserModel, UserWithPassword
from app.security import authenticate_user, get_token, get_current_active_user


app = FastAPI()


logger = logging.getLogger("api")
logger.setLevel(logging.DEBUG)

templates = Jinja2Templates(directory="templates")

engine = create_engine(f"{HOST}test_users_db", echo=True)
Session_search = sessionmaker(bind=engine)
s = Session_search()


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return await request_validation_exception_handler(request, exc)


@app.on_event("startup")
def startup():
    with open('logs/openapi.json', 'w') as out:
        json.dump(app.openapi(), out)


@app.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})


@app.get("/users", response_class=HTMLResponse)
async def user_list(request: Request):
    users = get_table(DatabaseUser, s)
    return templates.TemplateResponse("user_list.html", {"request": request, "users": users})


@app.get("/users/{user_login}", response_model=UserModel)
async def user_info(current_user: UserModel = Depends(get_current_active_user)):
    # TODO доработать обновление после смены данных
    return current_user


@app.post("/users/{user_login}")
async def edit_user(new_data: UserWithPassword, current_user: UserModel = Depends(get_current_active_user)):
    # TODO переделать кейс с None-полями
    if UserWithPassword is not None:
        edit_user_in_db(current_user.username, new_data, DatabaseUser, s)
        return {"status": "User " + current_user.username + " changed!"}
    else:
        return {"status": "User " + current_user.username + " wasn't changed"}


@app.post("/login", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(DatabaseUser, form_data.username, form_data.password)
    access_token = await get_token(user)
    return {"access_token": access_token, "token_type": "bearer"}


@app.post("/users/")
async def add_user(user_login: str, new_data: UserWithPassword):
    add_user_in_db(user_login, new_data, s)
    return {"status": "User " + user_login + " created!"}

if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8080, log_config="log.ini")
