from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db_template import DatabaseUser, UserBase
from os import environ
from datetime import date
from db_config import HOST

# DB_HOST = environ.get("DB_HOST", "mysql")

engine = create_engine(f"{HOST}", echo=True, pool_recycle=3600)
engine.execute('create database if not exists test_users_db')
engine = create_engine(f"{HOST}test_users_db?charset=utf8mb4", echo=False, pool_recycle=3600)


UserBase.metadata.create_all(engine)
metadata = UserBase.metadata
user_table = DatabaseUser.__table__

engine.execute("ALTER TABLE Users CONVERT TO CHARACTER SET utf8")

Session = sessionmaker(bind=engine)
s = Session()

if s.query(DatabaseUser).first() is None:
    print("Table is empty, adding users".upper(), flush=True)
    s.add(DatabaseUser(
        'Test_user_1',
        'John',
        date(2001, 12, 9),
        "$2b$12$mkNaJW4gq3iC3ud9D6dp2eZDGMn4J/u3WcM9fTx3RD.NyBxA92wh6"
    ))
    s.add(DatabaseUser(
        'Test_user_2',
        'Mary',
        date(1989, 2, 19),
        "$2b$12$mkNaJW4gq3iC3ud9D6dp2eZDGMn4J/u3WcM9fTx3RD.NyBxA92wh6"
    ))
    s.add(DatabaseUser(
        'Test_user_3',
        'Jane',
        date(2010, 3, 28),
        "$2b$12$mkNaJW4gq3iC3ud9D6dp2eZDGMn4J/u3WcM9fTx3RD.NyBxA92wh6"
    ))

    s.commit()
