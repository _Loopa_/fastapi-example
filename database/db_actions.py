# from sqlalchemy import create_engine
from database.db_template import DatabaseUser
from app.pydantic_models import UserInfo, UserWithPassword
from app.security_password import get_password_hash

# engine = create_engine('mysql://root:pass@mysql:3306/test_users_db', echo=True)


def get_full_info(required_username, user_table, session):
    found_user = session.query(user_table).filter_by(username=required_username).first()
    d = dict([
        ('username', found_user.username),
        ('hashed_password', found_user.hashed_password),
        ('name', found_user.name),
        ('birth_date', found_user.birth_date),
        ('disabled', found_user.disabled)
    ])
    return d


def search_by_username(required_username, user_table, session):
    found_user = session.query(user_table).filter_by(username=required_username).first()
    d = dict([('username', found_user.username), ('name', found_user.name), ('Birth date', found_user.birth_date)])
    return d


def add_user_in_db(new_username, newUserInfo: UserWithPassword, session):
    new_user = DatabaseUser(
        new_username, newUserInfo.name, newUserInfo.birth_date, get_password_hash(newUserInfo.password)
    )
    session.add(new_user)
    session.commit()


def edit_user_in_db(editable_user, newUserInfo: UserWithPassword, user_table, session):
    editing_user = session.query(user_table).filter_by(username=editable_user).first()
    editing_user.name = newUserInfo.name if newUserInfo.name is not None else None
    editing_user.birth_date = newUserInfo.birth_date if newUserInfo.birth_date is not None else None
    editing_user.hashed_password = get_password_hash(newUserInfo.password) if newUserInfo.password is not None else None
    session.commit()


def get_table(user_table, session):
    output = {}
    query = session.query(user_table)
    for user in query:
        output.update({user.username: {"name": user.name, "birth_date": user.birth_date}})
    return output
