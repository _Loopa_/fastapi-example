from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint, Date, Boolean
from sqlalchemy.ext.declarative import declarative_base


UserBase = declarative_base()


class DatabaseUser(UserBase):
    __tablename__ = 'Users'
    # __table_args__ = (
    #     PrimaryKeyConstraint('username', 'id'),
    # )
    # id = Column(Integer, primary_key=True)
    username = Column(String(50), primary_key=True, nullable=False)
    hashed_password = Column(String(100))
    name = Column(String(50))
    birth_date = Column(Date())
    disabled = Column(Boolean())

    def __init__(self, username, name, birth_date, hashed_password=None):
        self.username = username
        self.hashed_password = hashed_password
        self.name = name
        self.birth_date = birth_date
        self.disabled = False

    def __repr__(self):
        if not self.disabled:
            return "<User '%s' info:\nName: '%s'\nDate of Birth: '%s')>" % (self.username, self.name, self.birth_date)
        else:
            return "<User '%s' is disabled>" % self.username
