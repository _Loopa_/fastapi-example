from locust import HttpUser, between, task
from json import JSONDecodeError
import logging


class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def get_user_json(self):
        with self.client.get("/users/User_1", catch_response=True) as response:
            try:
                if response.json()["login"] != "User_1" or response.json()["name"] != "John":
                    response.failure("returned wrong value")
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")

    @task(4)
    def start_page(self):
        self.client.get("/")

    @task(3)
    def user_list(self):
        self.client.get("/users")
