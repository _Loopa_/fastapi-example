FROM python:3.10.0

WORKDIR /code

ENV DB_HOST="${DB_HOST}"
ENV SECRET_KEY_VED="${SECRET_KEY_VED}"

COPY ./requirements.txt /code/requirements.txt
RUN pip install  --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code/app
COPY ./database /code/database
COPY ./templates /code/templates
COPY ./logs /code/logs
COPY ./log.ini /code/log.ini

# RUN python -u ./database/db_init.py
CMD ["bash", "-c", "python database/db_init.py && uvicorn app.main:app --host 0.0.0.0 --port 8080 --log-config log.ini"]
